import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/interfaces/resume';
import { ResumeService } from 'src/app/services/http/resume/resume.service';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.sass']
})
export class ResumeComponent implements OnInit {
  showDropdownMenu = true
  data: Resume = {
    education: [],
    employment_history: [],
    external_link: [],
    professional_summary: [],
    personal_detail: [],
    skill: []
  }

  constructor(private resumeService: ResumeService) { }

  ngOnInit(): void {
    this.resumeService.get().subscribe(res => this.data = res)
  }

}
