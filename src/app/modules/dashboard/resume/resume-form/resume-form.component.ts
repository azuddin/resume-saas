import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResumeDataService } from 'src/app/services/data/resume/resume.service';
import { ResumeService } from 'src/app/services/http/resume/resume.service';

@Component({
  selector: 'app-resume-form',
  templateUrl: './resume-form.component.html',
  styleUrls: ['./resume-form.component.sass'],
})
export class ResumeFormComponent implements OnInit {
  @ViewChild('avatarHolder') avatarHolder!: ElementRef;
  @ViewChild('avatarInput') avatarInput!: ElementRef;

  mainForm!: FormGroup;

  get professional_summary(): FormArray {
    return this.mainForm.get('professional_summary') as FormArray;
  }

  get employment_history(): FormArray {
    return this.mainForm.get('employment_history') as FormArray;
  }

  get education(): FormArray {
    return this.mainForm.get('education') as FormArray;
  }

  get external_link(): FormArray {
    return this.mainForm.get('external_link') as FormArray;
  }

  get skill(): FormArray {
    return this.mainForm.get('skill') as FormArray;
  }

  constructor(private fb: FormBuilder, private resumeService: ResumeService, private resumeDS: ResumeDataService, private spinner: NgxSpinnerService, private router: Router) {}

  ngOnInit(): void {
    this.mainForm = this.fb.group({
      wantedjobtitle: '',
      avatar: '',
      firstname: '',
      lastname: '',
      email: '',
      phone: '',
      professional_summary: this.fb.array([]),
      employment_history: this.fb.array([]),
      education: this.fb.array([]),
      external_link: this.fb.array([]),
      skill: this.fb.array([]),
    });

    this.addStuff('professional_summary');


    this.resumeService.get().subscribe(res => {
      if (res.personal_detail[0]) {
        const pd = res.personal_detail[0]
        this.mainForm.patchValue({
          avatar: pd.avatar,
          email: pd.email,
          firstname: pd.firstname,
          id: pd.id,
          lastname: pd.lastname,
          phone: pd.phone,
        })
      }
    })
  }

  readAvatarUrl(input: any) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e) => {
        this.avatarHolder?.nativeElement.setAttribute('src', e.target?.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  onClickAvatar() {
    let el: HTMLElement = this.avatarInput?.nativeElement;
    el.click();
  }

  onChangeAvatar(e: Event) {
    this.readAvatarUrl(e.target);
  }

  onSubmit() {
    if (this.mainForm.valid) {
      console.log(this.mainForm.value);

      const formData = new FormData();
      formData.append('content', JSON.stringify(this.mainForm.value));
      formData.append('avatar', this.mainForm.get('avatar')?.value);

      this.spinner.show();
      setTimeout(() => {
        this.resumeService.save(formData).subscribe(res => {
          console.log(res)
          this.spinner.hide();
        }, error => {
          console.error(error)
        })
      }, 500);

    } else {
      this.getFormValidationErrors()
    }
  }

  getFormValidationErrors() {
    Object.keys(this.mainForm.controls).forEach((key) => {
      const controlErrors = this.mainForm.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          console.log(
            'Key control: ' + key + ', keyError: ' + keyError + ', err value: ',
            controlErrors[keyError]
          );
        });
      }
    });
  }

  newProSummary(): FormGroup {
    return this.fb.group({
      details: '',
    });
  }

  newEmpHistory(): FormGroup {
    return this.fb.group({
      jobtitle: '',
      employer: '',
      startdate: '',
      enddate: '',
      city: '',
      description: '',
    });
  }

  newEducation(): FormGroup {
    return this.fb.group({
      degree: '',
      school: '',
      startdate: '',
      enddate: '',
      city: '',
      description: '',
    });
  }

  newExtLink(): FormGroup {
    return this.fb.group({
      label: '',
      link: '',
    });
  }

  newSkill(): FormGroup {
    return this.fb.group({
      skillname: '',
      level: '',
    });
  }

  addStuff(type: string) {
    switch (type) {
      case 'professional_summary':
        this.professional_summary.push(this.newProSummary());
        break;
      case 'employment_history':
        this.employment_history.push(this.newEmpHistory());
        break;
      case 'education':
        this.education.push(this.newEducation());
        break;
      case 'external_link':
        this.external_link.push(this.newExtLink());
        break;
      case 'skill':
        this.skill.push(this.newSkill());
        break;
    }
  }

  removeStuff(type: string, i: number) {
    switch (type) {
      case 'professional_summary':
        this.professional_summary.removeAt(i);
        break;
      case 'employment_history':
        this.employment_history.removeAt(i);
        break;
      case 'education':
        this.education.removeAt(i);
        break;
      case 'external_link':
        this.external_link.removeAt(i);
        break;
      case 'skill':
        this.skill.removeAt(i);
        break;
    }
  }
}
