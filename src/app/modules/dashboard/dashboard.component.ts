import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  showDropdownMenu = true
  
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe(e => this.showDropdownMenu = true)
  }

}
