export interface Education {
  degree: string
  school: string
  startdate: string
  enddate: string
  city: string
  description: string
  order: number
}
