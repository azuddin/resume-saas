export interface ExternalLink {
  label: string
  link: string
  order: number
}
