export interface Skill {
  skillname: string
  level: number
  order: number
}
