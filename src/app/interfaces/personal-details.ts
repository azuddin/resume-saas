export interface PersonalDetails {
  avatar: string
  firstname: string
  lastname: string
  email: string
  phone: string
}
