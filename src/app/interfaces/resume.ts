import { Education } from "./education";
import { EmploymentHistory } from "./employment-history";
import { ExternalLink } from "./external-link";
import { PersonalDetails } from "./personal-details";
import { ProfessionalSummary } from "./professional-summary";
import { Skill } from "./skill";

export interface Resume {
  education: Education[];
  employment_history: EmploymentHistory[];
  external_link: ExternalLink[];
  professional_summary: ProfessionalSummary[];
  personal_detail: PersonalDetails[];
  skill: Skill[];
}
