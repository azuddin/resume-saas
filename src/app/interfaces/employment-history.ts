export interface EmploymentHistory {
  jobtitle: string
  employer: string
  startdate: string
  enddate: string
  city: string
  description: string
  order: number
}
