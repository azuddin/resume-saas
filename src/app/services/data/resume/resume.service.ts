import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Resume } from 'src/app/interfaces/resume';

@Injectable({
  providedIn: 'root'
})
export class ResumeDataService {
  public data$: BehaviorSubject<any> = new BehaviorSubject({})

  constructor() { }

  set setData(data: Resume) {
    this.data$.next(data)
  }
}
