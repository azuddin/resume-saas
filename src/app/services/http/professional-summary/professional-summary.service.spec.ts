import { TestBed } from '@angular/core/testing';

import { ProfessionalSummaryService } from './professional-summary.service';

describe('ProfessionalSummaryService', () => {
  let service: ProfessionalSummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfessionalSummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
