import { TestBed } from '@angular/core/testing';

import { EmploymentHistoryService } from './employment-history.service';

describe('EmploymentHistoryService', () => {
  let service: EmploymentHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmploymentHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
