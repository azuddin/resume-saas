import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/resume/1`)
  }

  save(data: any): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/resume/1`, data)
  }
}
